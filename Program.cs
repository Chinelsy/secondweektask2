﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;


namespace WeekendTwoTask
{
    class Program
    {
        static void Main(string[] args)
        {
            var name = new SortedList<string, int>();
            name.Add("Chikki", 1);
            name.Add("Eat", 2);
            name.Add("Rice", 3);
            name.Add("Yesterday", 6);
            name.Add("And", 4);
            name.Add("Stew", 5);

            Console.WriteLine("sorted========================");

            foreach (KeyValuePair<string, int> names in name)
            {
                Console.WriteLine($"{names.Key},{names.Value}");
            }

            Console.WriteLine("shuffled======================");

            Random random = new Random();

            var shuffle = name.OrderBy(x => random.Next());

            foreach (var names in shuffle)
            {
                Console.WriteLine($"{names}");
            }
        }


    }
}

